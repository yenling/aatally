/**
 * @jest-environment jsdom
 */

import { mount } from '@vue/test-utils'
import RecordEditorLoan from '../src/components/RecordEditorLoan.vue'

import CbModal from '../src/libs/cb-theme/Modal.vue'
import CbSelect from '../src/libs/cb-theme/Select.vue'
import CbRadio from '../src/libs/cb-theme/Radio.vue'

test('loan modal value', async () => {
  const wrapper = mount(RecordEditorLoan, {
    global: {
      components: {
        'cb-radio': CbRadio,
        'cb-select': CbSelect,
        'cb-modal': CbModal
      },
      provide: {
        UserStore: {
          state: {
            nickname: 'test01',
            friends: [{
              name: 'f01',
              id: ''
            }, {
              name: 'f02',
              id: ''
            }]
          }
        }
      }
    },
    propsData: {
      record: { amt: 100 }
    },
  })

  const userNickname = wrapper.get('#userNickname')

  expect(userNickname.text()).toBe('test01')
  expect(wrapper.componentVM.loanInfo.amt).toBe(100)
  expect(wrapper.componentVM.loanInfo.loaner.name).toBe('f01')

  // not work
  // const loanerSelect = wrapper.get('#loanerSelect')
  // expect(loanerSelect.text()).toBe('f01')
}, 2000)