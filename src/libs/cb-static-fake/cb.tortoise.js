
export class Tortoise {

    constructor(callback, options) {
        this.options = {
            delay: 100,
            mode: "last", /* last, series, pour_out */
            count_out: 0,
        };
        if (options) {
            if (options.delay) {
                this.options.delay = options.delay;
            }
            if (options.mode) {
                this.options.mode = options.mode;
            }
            if (options.count_out) {
                this.options.count_out = options.count_out;
            }
        }

        // -1: not in process, 0: waiting, 1: ready to execute
        this.flag = -1;
        this.callback = callback;
        this.timer = null;
        this.queue = [];
    };

    punch(...args) {
        var _equal = function(list1, list2) {
            if (list1.length == list2.length) {
                for (let _idx in list1) {
                    if (list1[_idx] !== list2[_idx]) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        };

        if (this.options.mode == "last") {
            this.queue.pop();
            this.queue.push(args);
        }
        else {
            let _has = false;
            for (let _argvs of this.queue) {
                if (_equal(_argvs, args)) {
                    _has = true;
                    break;
                }
            }
            if (!_has) {
                this.queue.push(args);
            }
        }

        return this._looping();
    };

    _looping() {
        var self = this;

        if (self.flag == 0) {
            if (self.options.count_out == 0 || self.queue.length < self.options.count_out) {
                if (self.timer) {
                    clearTimeout(self.timer);
                    self.timer = null;
                }
                self.flag = -1
            }
        }
        if (self.flag == -1) {
            self.flag = 0;
            self.timer = setTimeout(() => {
                self.exec()
            }, self.options.delay);
        }

        return self;
    };

    exec() {
        var self = this, args= null;

        if (self.timer) {
            clearTimeout(self.timer);
            self.timer = null;
        }

        if (self.flag == 0) {
            if (self.options.mode == "pour_out") {
                let all_args = [];
                for (let item of self.queue) {
                    all_args = all_args.concat(item);
                }
                self.queue = [];

                self.flag = 1;
                self.callback(all_args);
            }
            else {
                if (self.options.mode == "last") {
                    args = self.queue.pop();
                }
                else {
                    args = self.queue.shift();
                }

                if (args) {
                    self.flag = 1;
                    self.callback(...args);
                }
            }

            self.flag = -1;

            if (self.queue.length > 0) {
                self._looping();
            }
        }

        return self;
    };

};
