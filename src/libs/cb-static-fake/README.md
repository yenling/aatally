# cb-static
常用的 javascript 工具

## cb.util
- Array.deepClone
- Object.deepAssign
- Object.isDist: 檢查是否是 dict
- css_unit_join: default 'px'
```
css_unit_join(100) --> 100px
css_unit_join(100, 'em') --> 100em
```
- css_unit_split: 回傳一個 tuple [ int_val, str_unit ]
```
css_unit_split('100px') --> 100, px
css_unit_split('100%') --> 100, %
```
#### CBCSS: 這個還不知有沒有用...
- Unit: static method, 取得或設定全域 unit (default 'px')
-- CBCSS.Unit() --> 'px'
-- CBCSS.Unit('em') <-- 所有 CBCSS 的 default unit 都變成 em
- Parse: static method, CBCSS.Parse('100pt') --> 回傳新的 CBCSS object
```
import {CBCSS} from '../cb.util.js'

let css = CBCSS.Parse("200pt")
css.value += 100
css.toString() --> "300pt"
```

## cb.date
- clone
- eq, equal
- utcnow
- addSeconds, addMinutes, addHours, addDays
- diff
  - diffSeconds: 豪秒會被捨去
  - diffMinutes: 秒以下會被捨去
  - diffHours: 分以下會被捨去
  - diffDays: 小時以下會被捨去
- DTGFormat, setDTGFormat: 這個已經舊了, 現在儘量用 ISO Format
  - default "%Y%m%d%H%M"
  - using for dtgftimez, dtgptimez
- RegularFormat: setRegularFormat: 這個不是 ISO Format, 是顯示用的
  - default "%Y-%m-%d,%H:%M"
- setISOFormat: 用來指定 CB 常用的 ISOFormat, default "%Y-%m-%dT%H:%M", 專給 isoftimez 用 (時區就只有 +00:00)
- strftime, strptime: 用法與 Python 相同
- strftimez: 用法與 Python 相同, 專用於 UTC Time; 會用 getTimezoneOffset 轉成 UTC 時間
- setTimezone, getTimezone: 這個是擴充的資訊, 與 Date 原生的 TimezoneOffset 不一樣. TimezoneOffset 是瀏覽器自己的時區; setTimezone, getTimezone 是用來標示這個 Date 本身的資訊 (例如從 Server 取得的資料是 UTC Time 的時候)
- isostrz: 回傳 UTC ISO Format, default "%Y-%m-%dT%H:%M+00:00"
```
// 從 Server 取得的 datetime 是 UTC 時間
Date.strptimez(datetime, '%Y%m%d%H%M').toLST()  // <- 轉換成瀏覽器時區

// 現在儘量用 ISO Format 就好
// 例如從 Server 傳回的 datetime = "2020-04-10T16:00+00:00"
new Date(datetime).toLST()  // <- 轉換成瀏覽器時區

// 傳給 Server 用 isostrz 就好 (只傳 UTC Time)
new Date("2020-04-12T12:00+08:00").isostrz()  // <- return "2020-04-12T04:00+00:00"
```
- isUTC
- toUTC: 會去檢查 selfTimezone vs getTimezoneOffset, 依差異轉換成 UTC
- beUTC: 把 selfTimezone 設為 0
- toLST: 會去檢查 selfTimezone vs getTimezoneOffset, 依差異轉換成 LST
- beLST: 把 selfTimezone 設為 getTimezoneOffset
- zero_hour: 把小時、分鐘、秒、豪秒都設為 0 (取凌晨 00 時)
- zero_minute: 把分鐘、秒、豪秒都設為 0 (取整點時間)

## Tortoise
這個是用來延遲回應的元件, 主要用在平行序處理, 例如短時間多次觸發事件
例如把 options.mode 設為 last (default), 則在一段時間內多次觸發相同事件, 只有最後一次會被執行...

## cb.math
- intersect: 計算 2 條線是否交錯
