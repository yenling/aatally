export const convertInvoiceTime = (invoice) => {
  let tmp = invoice.invoiceTime.split(':')
  return new Date(invoice.invDate.year + 1911, invoice.invDate.month-1, invoice.invDate.date, tmp[0], tmp[1], tmp[2])
}
