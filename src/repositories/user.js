import { addDoc, collection, doc, getDoc, deleteDoc } from "firebase/firestore"
import db from "../connection/firebase"

export const UserRepo = {
  userId: {
    get: () => {
      return new Promise((resolve, reject) => {
        let uid = localStorage.getItem('tally-uid')
        if (uid) {
          resolve(uid)
        }
        else {
          reject(null)
        }
      })
    },

    post: () => {
      return new Promise(async (resolve, reject) => {
        const docRef = await addDoc(collection(db, "users"), {})
        localStorage.setItem('tally-uid', docRef.id)
        resolve(docRef.id)
      })
    }
  },

  nickname: {
    get: () => {
      return new Promise((resolve, reject) => {
        let nickname = localStorage.getItem('tally-nickname')
        if (nickname) {
          resolve(nickname)
        }
        else {
          reject(null)
        }
      })
    },

    put: (nickname) => {
      return new Promise((resolve, reject) => {
        try {
          localStorage.setItem('tally-nickname', nickname)
          resolve()
        }
        catch {
          reject()
        }
      })
    }
  },

  friends: {
    get: () => {
      return new Promise((resolve, reject) => {
        let currFriends = JSON.parse(localStorage.getItem('tally-friends'))
        if (currFriends) {
          resolve(currFriends)
        }
        else {
          reject()
        }
      })
    },
    post: (data) => {
      return new Promise((resolve, reject) => {
        try {
          let currFriends = JSON.parse(localStorage.getItem('tally-friends'))
          if (!currFriends) {
            currFriends = []
          }

          let targetIdx = currFriends.findIndex((f) => f.id === data.id)
          if (targetIdx >= 0) {
            currFriends[targetIdx] = data
          }
          else {
            currFriends.push({
              ...data,
              id: _getNextFriendId(currFriends)
            })
          }
          localStorage.setItem('tally-friends', JSON.stringify(currFriends))

          resolve(currFriends)
        } catch (err) {
          reject(err)
        }
      })
    },
    delete: (id) => {
      return new Promise((resolve, reject) => {
        try {
          let currFriends = JSON.parse(localStorage.getItem('tally-friends'))

          let target_idx = currFriends.findIndex((f) => f.id === id)
          currFriends.splice(target_idx, 1)

          localStorage.setItem('tally-friends', JSON.stringify(currFriends))

          resolve(currFriends)
        } catch (err) {
          reject(err)
        }
      })
    }
  },

  loanRecord: {
    delete: (uid, data) => {
      return new Promise(async (resolve, reject) => {
        await deleteDoc(doc(db, "users", uid, "loanRecord", data.id))
        resolve()
      })
    }
  },
}

const _getNextFriendId = (friends) => {
  if (friends.length === 0) {
    return 1
  }
  friends.sort((a, b) => a.id - b.id)

  return friends[friends.length - 1].id + 1
}
