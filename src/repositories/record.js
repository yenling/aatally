import { addDoc, collection } from "firebase/firestore"
import db from "../connection/firebase"

export const RecordRepo = {
  record: {
    get: () => {
      return new Promise((resolve, reject) => {
        let curr_records = JSON.parse(localStorage.getItem('tally-records'))
        if (curr_records) {
          resolve(curr_records)
        }
        else {
          reject()
        }
      })
    },
    post: (data) => {
      return new Promise((resolve, reject) => {
        try {
          let curr_records = JSON.parse(localStorage.getItem('tally-records'))
          if (!curr_records) {
            curr_records = []
          }

          if (data.id) {
            let target_idx = curr_records.findIndex(e => e.id === data.id)
            curr_records[target_idx] = data
          }
          else {
            curr_records.push({
              ...data,
              id: getNextRecordId(curr_records),
            })
          }
          localStorage.setItem('tally-records', JSON.stringify(curr_records))

          resolve(curr_records)
        } catch (err) {
          reject(err)
        }
      })
    },
    delete: (data) => {
      return new Promise((resolve, reject) => {
        try {
          let curr_records = JSON.parse(localStorage.getItem('tally-records'))

          let target_idx = curr_records.findIndex(e => e.id === data.id)
          curr_records.splice(target_idx, 1)

          localStorage.setItem('tally-records', JSON.stringify(curr_records))

          resolve(curr_records)
        } catch (err) {
          reject(err)
        }
      })
    }
  },

  loanRecord: {
    post: (uid, data) => {
      return new Promise(async (resolve, reject) => {
        await addDoc(collection(db, "users", uid, "loanRecord"), data)
        resolve()
      })
    }
  }
}

const getNextRecordId = (records) => {
  if (records.length === 0) {
    return 1
  }
  records.sort((a, b) => a.id - b.id)

  return records[records.length - 1].id + 1
}