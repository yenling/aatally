export const OthersRepo = {
  alias: {
    get: () => {
      return new Promise((resolve, reject) => {
        let curr_alias = JSON.parse(localStorage.getItem('tally-alias'))
        if (curr_alias) {
          resolve(curr_alias)
        }
        else {
          resolve({
            'self': '自己',
            'other': '對方'
          })
        }
      })
    },
    post: (data) => {
      return new Promise((resolve, reject) => {
        try {
          localStorage.setItem('tally-alias', JSON.stringify(data))

          resolve()
        } catch (err) {
          reject(err)
        }
      })
    }
  },

  tag: {
    get: () => {
      return new Promise((resolve, reject) => {
        let curr_tags = JSON.parse(localStorage.getItem('tally-tags'))
        if (curr_tags) {
          resolve(curr_tags)
        }
        else {
          reject()
        }
      })
    },
    post: (data) => {
      return new Promise((resolve, reject) => {
        try {
          localStorage.setItem('tally-tags', JSON.stringify(data))

          resolve()
        } catch (err) {
          reject(err)
        }
      })
    }
  }
}
