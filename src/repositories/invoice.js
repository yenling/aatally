import { req } from "../connection/aatally"

export const InvoiceRepo = {
  list: {
    post: (data) => req("post", 'invoice/list/', data),
  },
  detail: {
    post: (data) => req("post", 'invoice/detail/', data)
  },

  savedInvoiceList: {
    get: () => {
      return new Promise((resolve, reject) => {
        let invoiceList = localStorage.getItem('tally-invoiceList')
        if (invoiceList) {
          resolve(JSON.parse(invoiceList))
        }
        else {
          reject([])
        }
      })
    },
    put: (invoiceList) => {
      return new Promise((resolve, reject) => {
        try {
          localStorage.setItem('tally-invoiceList', JSON.stringify(invoiceList))
          resolve(invoiceList)
        }
        catch {
          reject()
        }
      })
    },
    delete: (invoice) => {
      return new Promise((resolve, reject) => {
        try {
          let invoiceList = JSON.parse(localStorage.getItem('tally-invoiceList'))

          let target_idx = invoiceList.findIndex(e => e.invNum === invoice.invNum)
          invoiceList.splice(target_idx, 1)

          localStorage.setItem('tally-invoiceList', JSON.stringify(invoiceList))

          resolve(invoiceList)
        }
        catch (error) {
          console.log('Failed to delete invoice.', error);
          reject()
        }
      })
    }
  },

  invoiceCardInfo: {
    get: () => {
      return new Promise((resolve, reject) => {
        let cardInfo = localStorage.getItem('tally-invoiceCardInfo')
        if (cardInfo) {
          resolve(JSON.parse(cardInfo))
        }
        else {
          reject(null)
        }
      })
    },
    put: (cardInfo) => {
      return new Promise((resolve, reject) => {
        try {
          localStorage.setItem('tally-invoiceCardInfo', JSON.stringify(cardInfo))
          resolve(cardInfo)
        }
        catch {
          reject()
        }
      })
    }
  },

  lastInvoiceInfo: {
    get: () => {
      return new Promise((resolve, reject) => {
        let lastInvoiceInfo = localStorage.getItem('tally-lastInvoiceInfo')
        if (lastInvoiceInfo) {
          resolve(JSON.parse(lastInvoiceInfo))
        }
        else {
          reject(null)
        }
      })
    },
    put: (lastInvoiceInfo) => {
      return new Promise((resolve, reject) => {
        try {
          localStorage.setItem('tally-lastInvoiceInfo', JSON.stringify(lastInvoiceInfo))
          resolve(lastInvoiceInfo)
        }
        catch {
          reject()
        }
      })
    }
  }
}
