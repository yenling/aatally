import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('./views/Home.vue')
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import('./views/Settings.vue')
  },
  {
    path: '/invoice',
    name: 'Invoice',
    component: () => import('./views/Invoice.vue')
  },
  {
    path: '/loan_record',
    name: 'LoanRecord',
    component: () => import('./views/LoanRecord.vue')
  }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

export default router
