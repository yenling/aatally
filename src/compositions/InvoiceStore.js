import { reactive, readonly, provide } from 'vue'
import { InvoiceRepo } from '../repositories/invoice'
import { cb_alert } from '../libs/cb-theme'
import { convertInvoiceTime } from '../utils/invoice'
import dayjs from 'dayjs'

export const useInvoiceStore = (UserStore) => {
  const state = reactive({
    invoiceCardInfo: null,
    invoiceList: null
  })

  const _initInvoiceCardInfo = () => {
    return InvoiceRepo.invoiceCardInfo.get()
      .then((cardInfo) => {
        state.invoiceCardInfo = cardInfo
      })
      .catch(() => {
        state.invoiceCardInfo = {}
      })
  }
  _initInvoiceCardInfo()

  const saveInvoiceCardInfo = (data) => {
    return InvoiceRepo.invoiceCardInfo.put(data)
      .then((cardInfo) => {
        state.invoiceCardInfo = cardInfo
      })
      .catch((error) => {
        console.warn(error)
      })
  }

  const _initInvoiceList = () => {
    return new Promise((resolve, reject) => {
      InvoiceRepo.savedInvoiceList.get()
        .then((invoiceList) => {
          state.invoiceList = invoiceList
        })
        .catch((error) => {
          console.warn(error)
          state.invoiceList = []
        })
    })
  }
  _initInvoiceList()

  const refreshInvoiceList = () => {
    if (!state.invoiceCardInfo.cardNo) return

    getLastInvoiceInfo()
      .then((lastInvoiceInfo) => {
        fetchNewInvoiceList(lastInvoiceInfo)
      })
  }

  const saveInvoiceList = (invoiceList) => {
    return InvoiceRepo.savedInvoiceList.put(invoiceList)
  }

  const removeInvoice = (invoice) => {
    return InvoiceRepo.savedInvoiceList.delete(invoice)
      .then((invoiceList) => {
        state.invoiceList = invoiceList
      })
  }

  const verifyInvoiceCard = (cardInfo) => {
    // 藉由取得發票列表成功與否來驗證載具資訊
    return InvoiceRepo.list.post({
      ...cardInfo,
      uuid: UserStore.state.uid,
      startDate: dayjs().add(-1, 'day').format('YYYY/MM/DD'),
      endDate: dayjs().format('YYYY/MM/DD'),
    })
      .then((payload) => {
        if (payload.data.code === 200) {
          return true
        }
        else {
          console.log('verify payload', payload)
          return false
        }
      })
      .catch((error) => {
        console.warn(error)
      })
  }

  const fetchNewInvoiceList = (lastInvoiceInfo) => {
    if (!lastInvoiceInfo) {
      lastInvoiceInfo = {
        date: dayjs().add(-7, 'day').format('YYYY/MM/DD'),
        timeStamp: 0
      }
    }
    return InvoiceRepo.list.post({
      cardNo: state.invoiceCardInfo.cardNo,
      cardEncrypt: state.invoiceCardInfo.cardEncrypt,
      cardType: state.invoiceCardInfo.cardType,
      uuid: UserStore.state.uid,
      startDate: lastInvoiceInfo.date,
      endDate: dayjs().format('YYYY/MM/DD'),
    })
      .then((payload) => {
        if (payload.data.code === 200) {
          // TODO: 處理發票時間比較
          let new_invoices = payload.data.details.filter((inv) => inv.invDate.time > lastInvoiceInfo.timeStamp)
          state.invoiceList = state.invoiceList.concat(new_invoices)

          saveInvoiceList(state.invoiceList)
            .then((invoiceList) => {
              let lastInvoice = invoiceList[invoiceList.length - 1]
              let lastInvoiceInfo = {
                date: dayjs(lastInvoice.invDate.time).format('YYYY/MM/DD'),
                timeStamp: lastInvoice.invDate.time
              }
              saveLastInvoiceInfo(lastInvoiceInfo)
            })
        }
        else {
          console.warn('Fail to fetch new invoice list.', payload.data)
          cb_alert('取得新發票失敗')
        }
      })
      .catch((error) => {
        console.warn('Fail to fetch new invoice list.', error)
        cb_alert('取得新發票失敗')
      })
  }

  const fetchInvoiceDetail = (invoice) => {
    return InvoiceRepo.detail.post({
      cardNo: state.invoiceCardInfo.cardNo,
      cardEncrypt: state.invoiceCardInfo.cardEncrypt,
      cardType: state.invoiceCardInfo.cardType,
      uuid: UserStore.state.uid,
      invNum: invoice.invNum,
      invDate: dayjs(convertInvoiceTime(invoice)).format('YYYY/MM/DD'),
      sellerName: invoice.sellerName,
      amount: invoice.amount
    })
      .then((payload) => {
        if (payload.data.code === 200) {
          return payload.data.details
        }
        else {
          console.warn('Fail to fetch invoice detail.', payload.data)
          cb_alert('取得發票明細失敗')
        }
      })
  }

  const getLastInvoiceInfo = () => {
    return InvoiceRepo.lastInvoiceInfo.get()
      .then((lastInvoiceInfo) => {
        return lastInvoiceInfo
      })
      .catch(() => {
        return null
      })
  }

  const saveLastInvoiceInfo = (data) => {
    return InvoiceRepo.lastInvoiceInfo.put(data)
      .catch((error) => {
        console.warn(error)
      })
  }


  const store = {
    state: readonly(state),
    refreshInvoiceList,
    fetchInvoiceDetail,
    verifyInvoiceCard,
    saveInvoiceCardInfo,
    removeInvoice,
    saveLastInvoiceInfo
  }

  provide('InvoiceStore', store)

  return store
}
