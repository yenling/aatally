import { reactive, readonly, provide } from 'vue'
import { UserRepo } from '../repositories/user'
import { onSnapshot, collection } from "firebase/firestore"
import db from "../connection/firebase"

export const useUserStore = () => {
  const state = reactive({
    uid: null,
    nickname: null,
    friends: null,
    loanRecords: null
  })

  const _initUid = () => {
    UserRepo.userId.get()
      .then((uid) => {
        state.uid = uid
        listenLoanRecord(uid)
      })
      .catch(() => {
        console.log("uid wrong")
        UserRepo.userId.post()
          .then((uid) => {
            state.uid = uid
          })
      })
  }
  _initUid()

  const listenLoanRecord = (uid) => {
    onSnapshot(collection(db, "users", uid, 'loanRecord'), (_coll) => {
      let _loanRecords = []
      _coll.forEach((_doc) => {
        _loanRecords.push({
          id: _doc.id,
          ..._doc.data()
        })
      })
      state.loanRecords = _loanRecords
    })
  }

  const _initNickname = () => {
    UserRepo.nickname.get()
      .then((nickname) => {
        state.nickname = nickname
      })
      .catch(() => {
        state.nickname = '自己'
      })
  }
  _initNickname()

  const saveNickname = (nickname) => {
    return UserRepo.nickname.put(nickname).then(() => {
      state.nickname = nickname
    })
  }

  const _initFriends = () => {
    UserRepo.friends.get()
      .then((friends) => {
        state.friends = friends
      })
      .catch(() => {
        state.friends = []
      })
  }
  _initFriends()

  const saveFriend = (data) => {
    return UserRepo.friends.post(data)
      .then((newFriends) => {
        state.friends = newFriends
      })
      .catch((error) => {
        console.warn(error)
      })
  }

  const removeFriend = (friend_id) => {
    return UserRepo.friends.delete(friend_id)
      .then((newFriends) => {
        state.friends = newFriends
      })
      .catch((error) => {
        console.warn(error)
      })
  }

  const checkUid = (uid) => {
    return UserRepo.userId.get(uid)
  }

  const removeLoanRecord = (data) => {
    return UserRepo.loanRecord.delete(state.uid, data)
  }

  const store = {
    state: readonly(state),
    saveFriend,
    removeFriend,
    checkUid,
    saveNickname,
    removeLoanRecord
  }

  provide('UserStore', store)

  return store
}
