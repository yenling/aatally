import { reactive, readonly, provide } from 'vue'
import { OthersRepo } from '../repositories/others'
import { DEFAULT_TAG_COLOR } from '../settings'

const DEFAULT_TAGS = [
  {
    text: '三餐',
    color: DEFAULT_TAG_COLOR
  },
  {
    text: '點心',
    color: DEFAULT_TAG_COLOR
  },
  {
    text: '服飾',
    color: DEFAULT_TAG_COLOR
  },
  {
    text: '交通',
    color: DEFAULT_TAG_COLOR
  }]

export const useOthersStore = () => {
  const state = reactive({
    tags: null
  })

  const getTags = () => {
    OthersRepo.tag.get()
      .then((tags) => {
        state.tags = tags
      })
      .catch(() => {
        state.tags = DEFAULT_TAGS
      })
  }
  getTags()

  const saveTags = (data) => {
    return OthersRepo.tag.post(data)
      .then(() => {
        state.tags = data
      })
  }

  const store = {
    state: readonly(state),
    saveTags
  }

  provide('OthersStore', store)

  return store
}
