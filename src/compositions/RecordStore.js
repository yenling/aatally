import { reactive, readonly, provide } from 'vue'
import { RecordRepo } from "../repositories/record"

export const useRecordStore = () => {
  const state = reactive({
  })

  const get_records_of_date = (date) => {
    return RecordRepo.record.get()
      .then((records) => {
        return records.map(r => {
          r.recordTime = new Date(r.recordTime)
          return r
        }).filter(record => {
          let recordValue = record.recordTime.valueOf()
          let dateValue = date.valueOf()
          if (recordValue >= dateValue && recordValue < dateValue + 24 * 60 * 60 * 1000) {
            return record
          }
        })
      })
      .catch(() => {
        return []
      })
  }

  const saveRecord = (record) => {
    return RecordRepo.record.post(record)
      .then((new_records) => {
        state.records = new_records
      })
  }

  const deleteRecord = (id) => {
    if (id) {
      return RecordRepo.record.delete({
        id
      })
        .then((new_records) => {
          state.records = new_records
        })
    }
    else {
      console.warn("id is required")
    }
  }

  const sendLoan = (uid, data) => {
    return RecordRepo.loanRecord.post(uid, data)
  }

  const store = {
    state: readonly(state),
    get_records_of_date,
    saveRecord,
    deleteRecord,
    sendLoan
  }

  provide('RecordStore', store)

  return store
}
