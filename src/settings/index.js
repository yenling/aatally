export const YEAR_START = 2000

export const YEAR_END = new Date().getFullYear() + 50

export const DEFAULT_TAG_COLOR = 'rgb(197, 216, 164)'
