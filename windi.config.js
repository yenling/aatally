const plugin = require('windicss/plugin')
const colors = require('windicss/colors')

module.exports = {
  extract: {
    include: ['./index.html', './src/**/*.{vue,js}'],
  },
  safelist: ['animate-spin', 'animate-pulse'],
  darkMode: false, // or 'media' or 'class',
  presets: [
    require('./src/libs/cb-theme/tw_preset.js')
  ],
  theme: {
    extend: {
      // 擴充 tailwind 預設的顏色
      colors: {
        gray: colors.trueGray,  // tailwind 預設的 gray 是 coolGray，這邊用 trueGray 取代掉

        primary: {
          DEFAULT: 'rgb(228, 174, 197)',
          light: 'rgb(250, 217, 230)',
          heavy: 'rgb(214, 133, 153)'
        },
        accent: {
          DEFAULT: 'rgb(144, 224, 239)',
          light: 'rgb(202, 240, 248)',
          heavy: 'rgb(0, 180, 216)'
        },
        special_red: 'rgb(199, 75, 80)',
        base: {
          DEFAULT: 'white'
        }
      },
      zIndex: {
        overlay: 100
      },
    },
  },
  shortcuts: {
    'vertical_center': 'absolute transform top-1/2 -translate-y-1/2',
    'horizontal_center': 'absolute transform left-1/2 -translate-x-1/2',
    'center': 'absolute transform top-1/2 left-1/2 -translate-y-1/2 -translate-x-1/2',
    'btn_disabled': 'bg-gray-200 text-gray-400 border border-solid border-gray-100 cursor-not-allowed'
  },
  plugins: [
    plugin(function ({ addUtilities, addVariant }) {
      addUtilities({
        '.scrollbar-hidden': {
          /* Firefox */
          'scrollbar-width': 'none',

          /* IE and Edge */
          '-ms-overflow-style': 'none',

          /* Safari and Chrome */
          '&::-webkit-scrollbar': {
            display: 'none'
          }
        }
      })
      addVariant('peer-checked', ({ modifySelectors }) => {
        return modifySelectors(({ className }) => {
          return `.peer:checked ~ .${className}`
        })
      })
      addVariant('peer-disabled', ({ modifySelectors }) => {
        return modifySelectors(({ className }) => {
          return `.peer:disabled ~ .${className}`
        })
      })
      addVariant('peer-focus', ({ modifySelectors }) => {
        return modifySelectors(({ className }) => {
          return `.peer:focus ~ .${className}`
        })
      })
    })
  ],
  corePlugins: {
    float: false
  }
}
